// SRS.csr16

// Test project for use with VM84 and VS2010
// added this line to test autocheckout in VS2010 with VM8.2 

// A main driver for the GUI version of the SRS.

// user name at login screen is 111-11-1111 or 222-22-2222 or 333-33-3333
// password is first three characters of the user id


using System;
using System.Windows.Forms;

public class SRS {

        // jpn code analysis suggestion for VS2012 - 08282012
    // Satisfies rule: MarkWindowsFormsEntryPointsWithStaThread.
    [STAThread]
  static void Main() {

    //  Create CourseCatalog, ScheduleOfClasses, and Faculty objects.
    //  The order of creation is important because a ScheduleOfClasses
    //  needs a CourseCatalog object to properly initialize and a 
    //  Faculty object needs a ScheduleOfClasses object.

    //  Create a CourseCatalog object and read data from input files. 

    CourseCatalog catalog = new CourseCatalog("CourseCatalog.dat", "Prerequisites.dat");
    catalog.ReadCourseCatalogData();
    catalog.ReadPrerequisitesData();

    //  Create a ScheduleOfClasses object and read data from input file.

    ScheduleOfClasses schedule = new ScheduleOfClasses("SoC_SP2009.dat", "SP2009");
    schedule.ReadScheduleData(catalog);

    //  Create a Faculty object and read data from input files.

    Faculty faculty = new Faculty("Faculty.dat", "TeachingAssignments.dat");
    faculty.ReadFacultyData();
    faculty.ReadAssignmentData(schedule);

    // Create and display an instance of the main GUI window

    Application.Run(new MainForm(schedule));
  }
}
