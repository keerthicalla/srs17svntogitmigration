:: [svn.project.name] build batch 
:: builds an SVN project locally
::
:: includes bootstrap to get build files from VM before calling actual build process
:: remark out the bootstrap call if you do not want to retrieve the build files from SVN to perserve your local working copy
::
:: search for [svn.project.name] and replace with the svn.project.name

:: BuildVersionNumber=build.version.number
:: BuildPreviousVersionNumber=buildversion.previous

echo off
cls

set svnprojectname=srs17
set buildfilename=TEA.Srs17Svn.build
echo Enter svn login id
set /p svnloginid=""
echo Enter svn password
set /p svnpassword=""
set emailaddress=jerry.norman@tea.texas.gov
set BuildVersionNumber=6.0.0.0
set BuildPreviousVersionNumber=Rel_6.0.0.0
set bvn=%BuildVersionNumber%

echo %BuildVersionNumber%
echo %BuildPreviousVersionNumber%

:: start in the project build folder

if not exist C:\Tea\%svnprojectname%\Build md cd C:\Tea\%svnprojectname%\Build
if not exist C:\Tea\%svnprojectname%\Build\Results md cd C:\Tea\%svnprojectname%\Build\Results

cd C:\Tea\%svnprojectname%\Build

echo Retrieving %svnprojectname% build file(s)

set nantver=92
call nant -buildfile:c:\tea\BuildIncludes\BootStrap.build bootStrapBuild -D:build.project.name=%svnprojectname% -D:svn.project.path="%svnprojectname%" -D:buildincludes.path=c:\tea\buildincludes\ -D:singletag=true -D:build.bootstrap.version.prefix=Rel_ -D:build.version.number=%BuildVersionNumber% -D:svn.userid=%svnloginid% -D:svn.password=%svnpassword% -D:email.build.results.to=%emailaddress% -logger:NAnt.Core.MailLogger 

echo starting to build project - press any key
pause
set nantver=92
call nant -buildfile:%buildfilename% buildMain -D:buildincludes.path=c:\tea\buildincludes\ -D:singletag=true -D:build.version.number=%BuildVersionNumber% -D:build.version.previous=%BuildPreviousVersionNumber% -D:svn.userid=%svnloginid% -D:svn.password=%svnpassword% -D:email.build.results.to=%emailaddress% -logger:NAnt.Core.MailLogger 

:: call nant -buildfile:%buildfilename% buildMain -D:build.version.number=%BuildVersionNumber% -D:build.version.previous=%BuildPreviousVersionNumber% -D:svn.userid=%svnloginid% -D:svn.password=%svnpassword% -D:email.build.results.to=%emailaddress% -logger:NAnt.Core.MailLogger -D:ivvBuild.Snk=true  -D:buildincludes.path=\\Tea4SvBuilder\buildincludes\

:: example of override the build include folder and the app staging server - notice the \\ after the buildinclude.path

:: nant -buildfile:TEA.SbecOnline.build buildMain -D:build.version.number=%BuildVersionNumber% -D:build.version.previous=%BuildPreviousVersionNumber% -D:svn.userid=%svnloginid% -D:svn.password=%svnpassword% -D:email.build.results.to=%emailaddress% -logger:NAnt.Core.MailLogger -D:buildincludes.path="\\[server or workstation]\c$\tea\buildincludes\\" -D:apps.server.name=[server or workstation]

pause
:: cleanup after build to prevent use of old build files next time
Call \\jnorman\c$\tea\buildincludes\Tea.Build.Cleanup.bat
:: pause

